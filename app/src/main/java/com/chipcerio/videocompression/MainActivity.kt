package com.chipcerio.videocompression

import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.otaliastudios.transcoder.Transcoder
import com.otaliastudios.transcoder.TranscoderListener
import com.otaliastudios.transcoder.TranscoderOptions
import com.otaliastudios.transcoder.sink.DataSink
import com.otaliastudios.transcoder.sink.DefaultDataSink
import com.otaliastudios.transcoder.source.DataSource
import com.otaliastudios.transcoder.source.TrimDataSource
import com.otaliastudios.transcoder.source.UriDataSource
import com.otaliastudios.transcoder.strategy.DefaultAudioStrategy
import com.otaliastudios.transcoder.strategy.DefaultVideoStrategy
import com.otaliastudios.transcoder.strategy.TrackStrategy
import com.otaliastudios.transcoder.strategy.size.FractionResizer
import com.otaliastudios.transcoder.strategy.size.PassThroughResizer
import java.io.File
import java.io.IOException
import java.util.concurrent.Future
import kotlin.math.roundToInt

@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity(), TranscoderListener {
    
    private lateinit var button: Button
    private lateinit var transcodeInputUri: Uri
    private lateinit var audioTrackStrategy: TrackStrategy
    private lateinit var videoTrackStrategy: TrackStrategy
    private lateinit var transcodeOutputFile: File
    private lateinit var transcodeFuture: Future<Void>
    
    private lateinit var progressView: ProgressDialog
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        button = findViewById(R.id.transcodeView)
        button.setOnClickListener {
            startActivityForResult(Intent(Intent.ACTION_GET_CONTENT).apply {
                type = "video/*"
                putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false)
            }, REQUEST_CODE_PICK)
        }
        
        progressView = ProgressDialog(this).apply {
            isIndeterminate = false
            max = PROGRESS_BAR_MAX
            setCancelable(false)
        }
    }
    
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_PICK &&
            resultCode == RESULT_OK &&
            data != null
        ) {
            data.data?.let {
                transcodeInputUri = it
                transcode()
            }
        }
    }
    
    private fun transcode() {
        // Create a temporary file for output.
        try {
            val outputDir = File(getExternalFilesDir(null), "outputs")
            outputDir.mkdir()
            transcodeOutputFile = File.createTempFile("transcode_test", ".mp4", outputDir)
        } catch (e: IOException) {
            System.err.println("Error in creating temporary file")
            return
        }
        
        val rotation = 0
        val speed = 1F
        setAudioTrackStrategy()
        setVideoTrackStrategy()
        
        // Launch the transcoding operation.
        val sink: DataSink = DefaultDataSink(transcodeOutputFile.absolutePath)
        val builder: TranscoderOptions.Builder = Transcoder.into(sink)
        
        val source: DataSource = UriDataSource(this, transcodeInputUri)
        builder.addDataSource(TrimDataSource(source, 0, 0))
        
        transcodeFuture = builder.apply {
            setListener(this@MainActivity)
            setAudioTrackStrategy(audioTrackStrategy)
            setVideoTrackStrategy(videoTrackStrategy)
            setVideoRotation(rotation)
            setSpeed(speed)
        }.transcode()
        progressView.show()
    }
    
    private fun setAudioTrackStrategy() {
        audioTrackStrategy = DefaultAudioStrategy.builder().apply {
            channels(DefaultAudioStrategy.CHANNELS_AS_INPUT)
            sampleRate(DefaultAudioStrategy.SAMPLE_RATE_AS_INPUT)
        }.build()
    }
    
    private fun setVideoTrackStrategy() {
        videoTrackStrategy = DefaultVideoStrategy.Builder().apply {
            addResizer(PassThroughResizer())
            addResizer(FractionResizer(1F))
            frameRate(DefaultVideoStrategy.DEFAULT_FRAME_RATE)
        }.build()
    }
    
    override fun onTranscodeCompleted(successCode: Int) {
        if (successCode == Transcoder.SUCCESS_TRANSCODED) {
            Toast.makeText(
                this,
                "Transcoded file placed on $transcodeOutputFile",
                Toast.LENGTH_LONG
            ).show()
            Log.d(TAG, "output: $transcodeOutputFile")
            progressView.dismiss()
        }
    }
    
    override fun onTranscodeProgress(progress: Double) {
        val progressToInt = (progress * PROGRESS_BAR_MAX).roundToInt()
        progressView.progress = progressToInt
        progressView.setMessage("${progressToInt / 10} % complete")
    }
    
    override fun onTranscodeCanceled() {
        Log.d(TAG, "canceled")
        progressView.dismiss()
    }
    
    override fun onTranscodeFailed(exception: Throwable) {
        Log.e(TAG, "Transcoding failed", exception)
        progressView.dismiss()
    }
    
    companion object {
        const val REQUEST_CODE_PICK = 1
        const val PROGRESS_BAR_MAX = 1000
        const val TAG = "MainActivity"
    }
}
